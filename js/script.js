$(document).ready(function () {

    $('.slider .owl-carousel').owlCarousel({
        loop:true,
        margin:0,
        nav:true,
        dots: false,
        navText: [],
        responsive:{
            0:{
                items:1,
                dots: true,
                nav: false,
            },
            480:{
                items:1
            }
        }
    })

    $('.featured .owl-carousel').owlCarousel({
        loop: true,
        margin: 30,
        nav: true,
        dots: false,
        navText: [],
        responsive: {
            0: {
                items: 1
            },
            480: {
                items: 2
            },
            680: {
                items: 3
            },
            1024: {
                items: 4
            }
        }
    })

    const body = document.querySelector('body')
    const iconSearch = document.querySelector('.icon__search')

    iconSearch.addEventListener('click', event => {
        event.stopPropagation()
        iconSearch.classList.add('icon__search--active')
    })

    body.addEventListener('click', () => iconSearch.classList.remove('icon__search--active'))
})